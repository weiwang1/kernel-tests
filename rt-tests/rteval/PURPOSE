The rteval script is a utility for measuring various aspects of
realtime behaviour on a system under load. The script unpacks the
hackbench and kernel source, builds hackbench and then goes into a
loop, running hackbench and compiling a kernel tree. During that loop
the cyclictest program is run to measure event response time. After
the run time completes, a statistical analysis of the event response
times is done and printed to the screen.

This script takes the following parameters:

* DURATION
  Default: 900
  Value passed to --duration of the rteval command

* LATCHECK
  Default: 1
  When set to 1, verify that rteval max latency does not exceed
  MAXLAT and standard deviation latency does not exceed STDDEVLAT.
  If any one of these values are exceeded, a FAIL result will be
  reported.

* MAXLAT
  Default: 150
  Maximum latency (in microseconds) that this system should not
  exceed.  LATCHECK=1 must be set to check this value.

* STDDEVLAT
  Default: 5
  Standard deviation latency (in microseconds) that this system
  should not exceed.  LATCHECK=1 must be set to check this value.

* LOADS_CPUS
  Default: "" (empty)
  If set, will run rteval with --loads-cpulist.  This can either
  be a CPU string (e.g. 1-9), "housekeeping" to run on housekeeping
  cores, or "isolated" to run on isolated cores.

* MEASURE_CPUS
  Default: "" (empty)
  If set, will run rteval with --measurement-cpulist.  This can
  either be a CPU string (e.g. 2-4), "housekeeping" to run on
  housekeeping cores, or "isolated" to run on isolated cores.
